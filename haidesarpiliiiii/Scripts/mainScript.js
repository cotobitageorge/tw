﻿var myGamePiece;
var myObstacles = [];
var myScore;
var scor;
var leaderboard = [];

if (localStorage.getItem("i" == null)) {
    localStorage.setItem("i", 0);
}
//console.log(localStorage.length);
for (var j = 0; j < localStorage.length - 1; j++) {
    leaderboard.push(JSON.parse(localStorage.getItem(j)));
}
//console.log(leaderboard[1]);

function sortare(a, b) {
    if (a.punctaj < b.punctaj) {
        return 1;
    }
    if (a.punctaj > b.punctaj) {
        return -1;
    }
    return 0;
}

function getLeaderboard() {
    leaderboard.sort(sortare);
    var top20 = [];
    for (var i = 0; i < 20; i++) {
        if (leaderboard[i] != null) {
            top20.push(leaderboard[i])
        }
        else {
            break;
        }
    }
    //console.log(top20);
    return top20;
}

function showTop() {
    var top20 = [];
    top20 = getLeaderboard();
    for (var i = 0; i < 20; i++) {
        if (top20[i] != null) {
            var nume;
            sir = top20[i].user;
            var scor;
            scor = top20[i].punctaj;

            //console.log(i + 1 + " " + sir + " " + scor);

            var loc = i + 1 + ". " + " " + sir + " " + scor;
            //console.log(loc);
            var t = document.createTextNode(loc);
            document.getElementById("top").appendChild(t);
            var br = document.createElement("br");
            document.getElementById("top").appendChild(br);
        }
    }
}


function startGame() {
    myGameArea.start();
    myObstacle = new component(10, 200, "green", 300, 120);
    myScore = new component("16px", "Lucida Console", "black", 380, 30, "text");
    myGamePiece = new component(30, 30, "rgb(102, 153, 255)", 10, 120);
}

var myGameArea = {
    canvas: document.createElement("canvas"),
    start: function () {
        this.canvas.width = 480;
        this.canvas.height = 270;
        this.context = this.canvas.getContext("2d");
        document.body.appendChild(this.canvas, document.getElementById("myBtn"));
        this.frameNo = 0;
        this.interval = setInterval(updateGameArea, 20);

        window.addEventListener('keydown', function (e) {
            myGameArea.keys = (myGameArea.keys || []);
            myGameArea.keys[e.keyCode] = (e.type == "keydown");
        })
        window.addEventListener('keyup', function (e) {
            myGameArea.keys[e.keyCode] = (e.type == "keydown");
        })
    },
    clear: function () {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    },
    stop: function () {
        clearInterval(this.interval);
        console.log(scor, "gata");
        var concurent = prompt("Game Over! You crashed the ship!" + "\nScore: " + scor + "\nEnter your name:");
        var i = localStorage.getItem("i");
        var player = { user: concurent, punctaj: scor };

        if (typeof (Storage) !== "undefined") {
            if (concurent == null || concurent == "") {
                location.reload();
            }
            else {
                localStorage.setItem(i, JSON.stringify(player));
                i++;
                localStorage.setItem("i", i);
                location.reload();
            }
        }
        
    }
}


function everyinterval(n) {
    if ((myGameArea.frameNo / n) % 1 == 0) { return true; }
    return false;
}

function component(width, height, color, x, y, type) {
    this.type = type;
    //this.gamearea = myGameArea;
    this.width = width;
    this.height = height;
    this.speedX = 0;
    this.speedY = 0;
    this.x = x;
    this.y = y;
    this.update = function () {
        ctx = myGameArea.context;
        if (this.type == "text") {
            ctx.font = this.width + " " + this.height;
            ctx.fillStyle = color;
            ctx.fillText(this.text, this.x, this.y);
        } else {
            ctx.fillStyle = color;
            ctx.fillRect(this.x, this.y, this.width, this.height);
        }
    }
    this.newPos = function () {
        this.x += this.speedX;
        this.y += this.speedY;
    }
    this.crashWith = function (otherobj) {
        var myleft = this.x;
        var myright = this.x + (this.width);
        var mytop = this.y;
        var mybottom = this.y + (this.height);
        var otherleft = otherobj.x;
        var otherright = otherobj.x + (otherobj.width);
        var othertop = otherobj.y;
        var otherbottom = otherobj.y + (otherobj.height);
        var crash = true;
        if ((mybottom < othertop) ||
            (mytop > otherbottom) ||
            (myright < otherleft) ||
            (myleft > otherright)) {
            crash = false;
        }
        return crash;

    }
}

function updateGameArea() {
    var x, y;
    for (i = 0; i < myObstacles.length; i += 1) {
        if (myGamePiece.crashWith(myObstacles[i])) {
            myGameArea.stop();
            return;
        }
    }
    myGameArea.clear();
    myGameArea.frameNo += 1;

    if (myGameArea.frameNo == 1 || everyinterval(100)) {
        x = myGameArea.canvas.width;
        minHeight = 50;
        maxHeight = 150;
        height = Math.floor(Math.random() * (maxHeight - minHeight + 1) + minHeight);
        minGap = 40;
        maxGap = 70;
        gap = Math.floor(Math.random() * (maxGap - minGap + 1) + minGap);
        myObstacles.push(new component(25, height, "rgb(232, 232, 232)", x, 0));
        myObstacles.push(new component(25, x - height - gap, "rgb(232, 232, 232)", x, height + gap));
    }

    //movement
    myGamePiece.speedX = 0;
    myGamePiece.speedY = 0;
    if (myGameArea.keys && myGameArea.keys[37]) { myGamePiece.speedX = -2; }
    if (myGameArea.keys && myGameArea.keys[39]) { myGamePiece.speedX = 2; }
    if (myGameArea.keys && myGameArea.keys[38]) { myGamePiece.speedY = -2; }
    if (myGameArea.keys && myGameArea.keys[40]) { myGamePiece.speedY = 2; }
    //movement end

    for (i = 0; i < myObstacles.length; i += 1) {
        myObstacles[i].x += -1;
        myObstacles[i].update();
    }

    myScore.text = "SCORE: " + myGameArea.frameNo;
    scor = parseInt(myGameArea.frameNo, 10);
    //console.log(scor);

    myScore.update();
    myGamePiece.newPos();
    myGamePiece.update();
}

